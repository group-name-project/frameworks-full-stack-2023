from flask import Flask, jsonify, request

app = Flask(__name__)

data = {
    "students": [
        {"name": "aluno 01", "id": 1},
        {"name": "aluno 02", "id": 2},
    ]
}

error_object = {
    "message": "Aluno não encontrado",
    "status_code": 404
}


@app.route("/v1/students", methods=["GET"])
def get_students():
    return jsonify(data)


@app.route("/v1/students/<int:id>", methods=["GET"])
def get_all_students(id):
    list_students = data["students"]

    for i in range(len(list_students)):
        item = list_students[i]
        if item["id"] == id:
            return jsonify(list_students[i])
    return (jsonify(error_object), 404)


if __name__ == '__main__':
    app.run(host='localhost', port=5002, debug=True)
